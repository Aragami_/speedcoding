﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalLightScript : MonoBehaviour
{
    private BoxCollider[] cubes;
    private GameObject myCube;
    private Rigidbody myCubeRig;
    private float speed = 200;

    private void Start()
    {
        // Find "TheCube"
        cubes = FindObjectsOfType<BoxCollider>();

        foreach (BoxCollider thisOne in cubes)
        {
            if (thisOne.gameObject.name == "TheCube")
            {
                myCube = thisOne.gameObject;
            }
        }

        myCubeRig = myCube.AddComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Vector3 dir = (Input.GetAxisRaw("Horizontal") * speed * Vector3.right * Time.deltaTime + Input.GetAxisRaw("Vertical") * speed * Vector3.forward * Time.deltaTime);
        dir.y = myCubeRig.velocity.y;
        myCubeRig.velocity = dir;

        if (Input.GetKey(KeyCode.Space))
        {
            myCubeRig.AddForce(Vector3.up * 20);
        }
    }
}
