﻿using System;

namespace SpeedCoding03
{
    class Program
    {
        static void Main(string[] args)
        {
            // 5, 2, -7, 3, -2, 5, 4, -4
            int[] myInt = { 5, 2, -7, 3, -2, 5, 4, -4};
            Console.WriteLine(GetHighestInArray(myInt));
                
        }

        static int Betrag(int _zahl)
        {
            if (_zahl < 0)
            {
                return _zahl * -1;
            }
            else
            {
                return _zahl;
            }
        }

        static int GetHighestInArray(int[] _a)
        {
            int highest = _a[0];
            for (int i = 1; i < _a.Length; i++)
            {
                if (Betrag(highest) < Betrag(_a[i]))
                {
                    //if (highest == Betrag(_a[i]))
                    //{
                    //    highest = Betrag(_a[i]);
                    //}
                    //else
                    highest = _a[i];
                }
                else
                {
                    if (Betrag(highest) == Betrag(_a[i]))
                    {
                        highest = Math.Max(highest, _a[i]);
                    }
                }

            }
            return highest;
        }
    }
}
